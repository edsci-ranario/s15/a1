console.log('Hello, World')
// Details
const details = {
    fName: "Arjay",
    lName: "Ranario",
    age: 18,
    hobbies : [
        "gaming", "designing my room", "drawing"
    ] ,
    workAddress: {
        housenumber: "Lot 2, Block 129",
        street: "Deez",
        city: "Knotz",
        state: "Joklang",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");